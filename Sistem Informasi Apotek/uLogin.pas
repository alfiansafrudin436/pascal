unit uLogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, acPNG;

type
  Tform_Login = class(TForm)
    panel1: TPanel;
    GroupBox1: TGroupBox;
    username: TEdit;
    pass: TEdit;
    Button1: TButton;
    Image1: TImage;
    Image2: TImage;
    Button2: TButton;
    Label1: TLabel;
    Label2: TLabel;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_Login: Tform_Login;

implementation

uses uMain, uLamanPertama, uDM, uAwal;

{$R *.dfm}

procedure Tform_Login.Button2Click(Sender: TObject);
begin
form_Login.Hide;
form_awal.Show;
username.Text:='';
pass.Text:='';
end;

procedure Tform_Login.Button1Click(Sender: TObject);
begin
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='Select * from tbl_karyawan';
  ExecSQL;
  Active:=True;
if (username.Text='' )OR (pass.Text='') then
  begin
    ShowMessage('Username dan Password tidak boleh kosong');
  end
else if (username.Text<>DM.ADO_Query['username'] )OR (pass.Text<>DM.ADO_Query['password'] ) then
  begin
    ShowMessage('Username atau Password Salah');
  end;
if (username.Text=DM.ADO_Query['username'] )and (pass.Text=DM.ADO_Query['password'] ) then
  begin
    form_main.Show;
    form_Login.Hide;
    username.Text:='';
    pass.Text:='';
  end ;
end;
end;

procedure Tform_Login.FormActivate(Sender: TObject);
begin
 Left:=(Screen.Width-Width)  div 2;
 Top:=(Screen.Height-Height) div 2;
end;

end.
