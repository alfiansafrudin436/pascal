unit uLamanPertama;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, acPNG, ExtCtrls;

type
  Tform_awal = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Image1: TImage;
    Image2: TImage;
    Label4: TLabel;
    Label1: TLabel;
    Image3: TImage;
    procedure Panel2Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_awal: Tform_awal;

implementation

uses uLogin2, uLogin;

{$R *.dfm}

procedure Tform_awal.Panel2Click(Sender: TObject);
begin
form_LoginAdmin.Show;
form_awal.Hide;
end;

procedure Tform_awal.Image1Click(Sender: TObject);
begin
form_LoginAdmin.Show;
form_awal.Hide;
end;

procedure Tform_awal.Label4Click(Sender: TObject);
begin
form_LoginAdmin.Show;
form_awal.Hide;
end;

procedure Tform_awal.Image2Click(Sender: TObject);
begin
form_Login.Show;
form_awal.Hide;
end;

procedure Tform_awal.Panel3Click(Sender: TObject);
begin
form_Login.Show;
form_awal.Hide;
end;

procedure Tform_awal.Label1Click(Sender: TObject);
begin
form_Login.Show;
form_awal.Hide;
end;

procedure Tform_awal.FormActivate(Sender: TObject);
begin
 Left:=(Screen.Width-Width)  div 2;
 Top:=(Screen.Height-Height) div 2;
end;

end.
