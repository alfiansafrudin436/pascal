unit uProsesTransaksi;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls;

type
  TForm_transaksi = class(TForm)
    GroupBox1: TGroupBox;
    DBGrid1: TDBGrid;
    Label18: TLabel;
    GroupBox2: TGroupBox;
    Dibayar: TLabel;
    Label1: TLabel;
    lbl_kembali: TLabel;
    Label2: TLabel;
    lbl_harga: TLabel;
    edt_Dibayar: TEdit;
    btn_Simpan: TButton;
    Button1: TButton;
    Button2: TButton;
    procedure FormActivate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btn_SimpanClick(Sender: TObject);
    procedure RefreshTrans;
    procedure edt_DibayarChange(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form_transaksi: TForm_transaksi;

implementation

uses uDM, uMain, ADODB, rep;

{$R *.dfm}

procedure TForm_transaksi.RefreshTrans;
begin
DM.DS.DataSet:=DM.ADO_Query;
DBGrid1.DataSource:=DM.DS;
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
  begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select * from tbl_temporary';
  Active:=True;
  end;
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query1 do
  begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='SELECT SUM(Harga_Total) as Total FROM tbl_temporary';
  Active:=True;
end;
end;

procedure TForm_transaksi.FormActivate(Sender: TObject);
begin
DM.DS.DataSet:=DM.ADO_Query;
DBGrid1.DataSource:=DM.DS;
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
  begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select ID_Jual, ID_Obat, Nama_Obat, Jenis, Tanggal, QTY, Satuan, Total_Harga, Nama_Karyawan from Transaksi_Jual';
  Active:=True;
  end;
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query1 do
  begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='SELECT SUM(Total_Harga) as Total FROM Transaksi_Jual';
  Active:=True;
end;

 Left:=(Screen.Width-Width)  div 2;
 Top:=(Screen.Height-Height) div 2;


lbl_harga.Caption:=IntToStr(DM.ADO_Query1['Total'])
end;

procedure TForm_transaksi.Button1Click(Sender: TObject);
begin
form_main.Show;
form_transaksi.Hide;
form_main.grubox_transaksi_jual.Show;
with DM.ADO_Query1 do
end;

procedure TForm_transaksi.btn_SimpanClick(Sender: TObject);
var a,b,c:integer;
begin
a:=StrToInt(lbl_harga.Caption);
b:=StrToInt(edt_Dibayar.Text);
c:=b-a;
lbl_kembali.Caption:=IntToStr(c);
with DM.ADO_Query1 do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select*from Transaksi_Jual';
  Active:=True;

Form1.QuickRep1.DataSet:=DM.ADO_Query1;
Form1.ID.DataSet:=DM.ADO_Query1;      Form1.ID.DataField:='Id_Obat';
Form1.NAMA.DataSet:=DM.ADO_Query1;    Form1.NAMA.DataField:='Nama_Obat';
Form1.JENIS.DataSet:=DM.ADO_Query1;   Form1.JENIS.DataField:='Jenis';
Form1.HARGA.DataSet:=DM.ADO_Query1;   Form1.HARGA.DataField:='Satuan';
Form1.QTY.DataSet:=DM.ADO_Query1;     Form1.QTY.DataField:='QTY';
Form1.QRDBText1.DataSet:=DM.ADO_Query1;  Form1.QRDBText1.DataField:='Total_Harga';
Form1.KARYAWAN.DataSet:=DM.ADO_Query1;  Form1.KARYAWAN.DataField:='Nama_Karyawan';
Form1.QRDBText2.DataSet:=DM.ADO_Query1;  Form1.QRDBText2.DataField:='Tanggal';
Form1.QRLabel9.Caption:=IntToStr(a);
Form1.QRLabel10.Caption:=IntToStr(b);
Form1.QRLabel11.Caption:=IntToStr(c);
Form1.QRLabel9.Caption:=IntToStr(a);

end;
Form1.QuickRep1.Preview;
with DM.ADO_Query1 do

RefreshTrans;
end;

procedure TForm_transaksi.edt_DibayarChange(Sender: TObject);
var a,b,c:integer;
begin
a:=StrToInt(lbl_harga.Caption);
b:=StrToInt(edt_Dibayar.Text);
c:=b-a;
lbl_kembali.Caption:=IntToStr(c);
if(b<a)then
lbl_kembali.Caption:='Uang Kurang';
end;

procedure TForm_transaksi.Button2Click(Sender: TObject);
begin
with DM.ADO_Query1 do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='exec delete_temp';
  ExecSQL;
  Form_transaksi.Hide;
end;
end;
end.
