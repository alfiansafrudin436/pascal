unit uMainAdmin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sSkinManager, ImgList, acPNG, ExtCtrls, StdCtrls, Grids,
  DBGrids, ComCtrls;

type
  Tform_main_Admin = class(TForm)
    page: TPageControl;
    page_master: TTabSheet;
    grubox_master_data_karyawan: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBGrid1: TDBGrid;
    Button5: TButton;
    Button4: TButton;
    Button3: TButton;
    edt_Alamat: TEdit;
    edt_telp: TEdit;
    edt_Nama: TEdit;
    edt_ID: TEdit;
    Panel1: TPanel;
    Label1: TLabel;
    Panel4: TPanel;
    Image3: TImage;
    Label10: TLabel;
    cbb_jk: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    edt_username: TEdit;
    edt_pass: TEdit;
    Panel7: TPanel;
    Panel5: TPanel;
    Image4: TImage;
    Label13: TLabel;
    Button1: TButton;
    Image1: TImage;
    Label17: TLabel;
    grubox_master_data_admin: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    DBGrid2: TDBGrid;
    edt_usernameAdmin: TEdit;
    edt_passAdmin: TEdit;
    EDIT: TButton;
    CLEAR: TButton;
    DELETE: TButton;
    edt_idAdmin: TEdit;
    procedure Refresh;
    procedure FormActivate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Label13Click(Sender: TObject);
    procedure Image4Click(Sender: TObject);
    procedure Panel5Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
    procedure Panel4Click(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure Panel7Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure DBGrid2CellClick(Column: TColumn);
    procedure EDITClick(Sender: TObject);
    procedure CLEARClick(Sender: TObject);
    procedure DELETEClick(Sender: TObject);
    procedure Label17Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_main_Admin: Tform_main_Admin;

implementation

uses uDM, uLamanPertama;

{$R *.dfm}
procedure Tform_main_Admin.Refresh;
begin
DM.DS.DataSet:=DM.ADO_Query;
DBGrid1.DataSource:=DM.DS;
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select * from tbl_karyawan';
  Active:=True;
end;
end;

procedure Tform_main_Admin.FormActivate(Sender: TObject);
begin
Refresh;
grubox_master_data_karyawan.Hide;
grubox_master_data_admin.Hide;
Left:=(Screen.Width-Width)  div 2;
Top:=(Screen.Height-Height) div 2;

end;

procedure Tform_main_Admin.DBGrid1CellClick(Column: TColumn);
begin
edt_ID.Text:=DM.ADO_Query['ID_Karyawan'];
edt_Nama.Text:=DM.ADO_Query['Nama_Karyawan'];
cbb_jk.Text:=DM.ADO_Query['Jenis_kelamin'];
edt_telp.Text:=DM.ADO_Query['Telp'];
edt_Alamat.Text:=DM.ADO_Query['Alamat'];
edt_username.Text:=DM.ADO_Query['username'];
edt_pass.Text:=DM.ADO_Query['password'];
edt_ID.Enabled:=False;
end;

procedure Tform_main_Admin.Button4Click(Sender: TObject);
begin
If ((edt_ID.Text='') and (edt_Nama.Text='') and (cbb_jk.Text='') and (edt_telp.Text='') and (edt_Alamat.Text='')and (edt_username.Text='')and (edt_pass.Text='')) then
  begin
  ShowMessage('Data Masih Kosong');
  end
  else
  try DM.ADO_Con.BeginTrans;
  with DM.ADO_Query do
  begin
  Active:=False;
  SQL.Clear;
  SQL.Text:='update tbl_karyawan set Nama_Karyawan='+QuotedStr(edt_Nama.Text)+
  ',Jenis_kelamin='+QuotedStr(cbb_jk.Text)+
  ',Telp='+QuotedStr(edt_telp.Text)+
  ',Alamat='+QuotedStr(edt_Alamat.Text)+
  ',username='+QuotedStr(edt_username.Text)+
  ',password='+QuotedStr(edt_pass.Text)+
  'where ID_Karyawan='+QuotedStr(edt_ID.Text);
  ExecSQL;
  end;
  DM.ADO_Con.CommitTrans;
  ShowMessage('Data Berhasil diubah');
  edt_ID.Text:='';
  edt_Nama.Text:='';
  cbb_jk.Text:='';
  edt_telp.Text:='';
  edt_Alamat.Text:='';
  edt_username.Text:='';
  edt_pass.Text:='';
  Refresh;
  except
  ShowMessage('Data Gagal diubah');
  DM.ADO_Con.RollbackTrans;
  end;
end;

procedure Tform_main_Admin.Button3Click(Sender: TObject);
begin
If ((edt_ID.Text='') and (edt_Nama.Text='') and (cbb_jk.Text='') and (edt_telp.Text='') and (edt_Alamat.Text='')and (edt_username.Text='')and (edt_pass.Text='')) then
  begin
  ShowMessage('Data Masih Kosong');
  end
else
  try DM.ADO_Con.BeginTrans;
  with DM.ADO_Query do
  begin
  Active:=False;
  SQL.Clear;
  SQL.Text:='insert into tbl_karyawan values('+QuotedStr(edt_ID.Text)+','+
  QuotedStr(edt_Nama.Text)+','+QuotedStr(cbb_jk.Text)+','+
  QuotedStr(edt_telp.Text)+','+QuotedStr(edt_Alamat.Text)+','+
  QuotedStr(edt_username.Text)+','+QuotedStr(edt_pass.Text)+')';
  ExecSQL;
  end;
  DM.ADO_Con.CommitTrans;
  ShowMessage('Data Berhasil Disimpan');
  except
  ShowMessage('Data Gagal Disimpan');
  DM.ADO_Con.RollbackTrans;
  end;
  edt_ID.Text:='';
  edt_Nama.Text:='';
  cbb_jk.Text:='';
  edt_telp.Text:='';
  edt_Alamat.Text:='';
  edt_username.Text:='';
  edt_pass.Text:='';
  Refresh;
end;

procedure Tform_main_Admin.Button5Click(Sender: TObject);
begin
If ((edt_ID.Text='') and (edt_Nama.Text='') and (cbb_jk.Text='') and (edt_telp.Text='') and (edt_Alamat.Text='')and (edt_username.Text='')and (edt_pass.Text='')) then
  begin
  ShowMessage('Data Masih Kosong');
  end
else
  try DM.ADO_Con.BeginTrans;
  with DM.ADO_Query do
  begin
  Active:=False;
  SQL.Clear;
  SQL.Text:='delete from tbl_karyawan where ID_Karyawan='+QuotedStr(edt_ID.Text);
  ExecSQL;
  end;
  DM.ADO_Con.CommitTrans;
  ShowMessage('Data Berhasil Dihapus');
  except
  ShowMessage('Data Gagal Dihapus');
  DM.ADO_Con.RollbackTrans;
  end;
  edt_ID.Text:='';
  edt_Nama.Text:='';
  cbb_jk.Text:='';
  edt_telp.Text:='';
  edt_Alamat.Text:='';
  edt_username.Text:='';
  edt_pass.Text:='';
  Refresh;
end;

procedure Tform_main_Admin.Label13Click(Sender: TObject);
begin
form_awal.Show;
form_main_Admin.Hide;
end;

procedure Tform_main_Admin.Image4Click(Sender: TObject);
begin
form_awal.Show;
form_main_Admin.Hide;
end;

procedure Tform_main_Admin.Panel5Click(Sender: TObject);
begin
form_awal.Show;
form_main_Admin.Hide;
end;

procedure Tform_main_Admin.Button1Click(Sender: TObject);
begin
  edt_ID.Text:='';
  edt_Nama.Text:='';
  cbb_jk.Text:='';
  edt_telp.Text:='';
  edt_Alamat.Text:='';
  edt_username.Text:='';
  edt_pass.Text:='';
  edt_ID.Enabled:=True;
end;

procedure Tform_main_Admin.Image3Click(Sender: TObject);
begin
grubox_master_data_karyawan.Show;
grubox_master_data_admin.Hide;
Refresh;
end;

procedure Tform_main_Admin.Panel4Click(Sender: TObject);
begin
grubox_master_data_karyawan.Show;
grubox_master_data_admin.Hide;
Refresh;
end;

procedure Tform_main_Admin.Label10Click(Sender: TObject);
begin
grubox_master_data_karyawan.Show;
grubox_master_data_admin.Hide;
Refresh;
end;

procedure Tform_main_Admin.Panel7Click(Sender: TObject);
begin
grubox_master_data_admin.Show;
grubox_master_data_karyawan.Hide;

DM.DS.DataSet:=DM.ADO_Query;
DBGrid2.DataSource:=DM.DS;
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select * from tbl_Admin';
  Active:=True;
end;
end;

procedure Tform_main_Admin.Image1Click(Sender: TObject);
begin
grubox_master_data_admin.Show;
grubox_master_data_karyawan.Hide;

DM.DS.DataSet:=DM.ADO_Query;
DBGrid2.DataSource:=DM.DS;
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select * from tbl_Admin';
  Active:=True;
end;
end;

procedure Tform_main_Admin.DBGrid2CellClick(Column: TColumn);
begin
edt_idAdmin.Text:=DM.ADO_Query['ID_Admin'];
edt_usernameAdmin.Text:=DM.ADO_Query['username'];
edt_passAdmin.Text:=DM.ADO_Query['password'];
edt_idAdmin.Enabled:=False;
end;

procedure Tform_main_Admin.EDITClick(Sender: TObject);
begin
If ((edt_idAdmin.Text='') and (edt_usernameAdmin.Text='') and (edt_passAdmin.Text='')) then
  begin
  ShowMessage('Data Masih Kosong');
  end
  else
  try DM.ADO_Con.BeginTrans;
  with DM.ADO_Query do
  begin
  Active:=False;
  SQL.Clear;
  SQL.Text:='delete from tbl_obat where id_obat='+QuotedStr(edt_ID.Text);
  ExecSQL;
  end;
  DM.ADO_Con.CommitTrans;
  ShowMessage('Data Berhasil diubah');
  edt_idAdmin.Text:='';
  edt_usernameAdmin.Text:='';
  edt_passAdmin.Text:='';
  Refresh;
  except
  ShowMessage('Data Gagal diubah');
  DM.ADO_Con.RollbackTrans;
end;
end;

procedure Tform_main_Admin.CLEARClick(Sender: TObject);
begin
  edt_idAdmin.Text:='';
  edt_usernameAdmin.Text:='';
  edt_passAdmin.Text:='';
end;

procedure Tform_main_Admin.DELETEClick(Sender: TObject);
begin
If ((edt_idAdmin.Text='') and (edt_usernameAdmin.Text='') and (edt_passAdmin.Text='')) then
  begin
  ShowMessage('Data Masih Kosong');
  end
  else
  try DM.ADO_Con.BeginTrans;
  with DM.ADO_Query do
  begin
  Active:=False;
  SQL.Clear;
  SQL.Text:='update tbl_Admin set username='+QuotedStr(edt_usernameAdmin.Text)+
  ',Jenis='+QuotedStr(edt_passAdmin.Text)+
  'where ID_Admin='+QuotedStr(edt_idAdmin.Text);
  ExecSQL;
  end;
  DM.ADO_Con.CommitTrans;
  ShowMessage('Data Berhasil diubah');
  edt_idAdmin.Text:='';
  edt_usernameAdmin.Text:='';
  edt_passAdmin.Text:='';
  Refresh;
  except
  ShowMessage('Data Gagal diubah');
  DM.ADO_Con.RollbackTrans;
end;
end;

procedure Tform_main_Admin.Label17Click(Sender: TObject);
begin
grubox_master_data_admin.Show;
grubox_master_data_karyawan.Hide;

DM.DS.DataSet:=DM.ADO_Query;
DBGrid2.DataSource:=DM.DS;
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select * from tbl_Admin';
  Active:=True;
end;
end;

end.
