unit uLogin2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, acPNG, ExtCtrls;

type
  Tform_LoginAdmin = class(TForm)
    panel1: TPanel;
    GroupBox1: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    username: TEdit;
    pass: TEdit;
    Button1: TButton;
    Button2: TButton;
    Label12: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_LoginAdmin: Tform_LoginAdmin;

implementation

uses uLamanPertama, uDM, uMain, uLogin, uMainAdmin, ADODB;

{$R *.dfm}

procedure Tform_LoginAdmin.Button2Click(Sender: TObject);
begin
form_LoginAdmin.Hide;
form_awal.Show;
username.Text:='';
pass.Text:='';
end;

procedure Tform_LoginAdmin.Button1Click(Sender: TObject);
begin
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='Select * from tbl_admin'; 
  ExecSQL;
  Active:=True;


if (username.Text='' )OR (pass.Text='') then
  begin
    ShowMessage('Username dan Password tidak boleh kosong');
    username.Text:='';
    pass.Text:='';
  end
else if (username.Text<>DM.ADO_Query['username'] )OR (pass.Text<>DM.ADO_Query['password'] ) then
  begin
    ShowMessage('Username atau Password Salah');
    username.Text:='';
    pass.Text:='';
  end;
if (username.Text=DM.ADO_Query['username'] )and (pass.Text=DM.ADO_Query['password'] ) then
  begin
    form_main_Admin.Show;
    form_LoginAdmin.Hide;
    username.Text:='';
    pass.Text:='';
  end ;
end;
end;
procedure Tform_LoginAdmin.FormActivate(Sender: TObject);
begin
  Left:=(Screen.Width-Width)  div 2;
  Top:=(Screen.Height-Height) div 2;
end;

end.
