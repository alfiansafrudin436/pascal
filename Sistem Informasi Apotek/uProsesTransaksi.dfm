object Form_transaksi: TForm_transaksi
  Left = 95
  Top = 127
  Width = 849
  Height = 558
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 545
    Height = 497
    TabOrder = 0
    object Label18: TLabel
      Left = 208
      Top = 32
      Width = 155
      Height = 24
      Caption = 'TRANSAKSI JUAL'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBGrid1: TDBGrid
      Left = 16
      Top = 80
      Width = 513
      Height = 249
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object GroupBox2: TGroupBox
    Left = 552
    Top = 0
    Width = 273
    Height = 497
    TabOrder = 1
    object Dibayar: TLabel
      Left = 24
      Top = 160
      Width = 36
      Height = 13
      Caption = 'Dibayar'
    end
    object Label1: TLabel
      Left = 24
      Top = 208
      Width = 46
      Height = 13
      Caption = 'KEMBALI'
    end
    object lbl_kembali: TLabel
      Left = 112
      Top = 208
      Width = 48
      Height = 13
      Caption = '00000000'
    end
    object Label2: TLabel
      Left = 24
      Top = 112
      Width = 35
      Height = 13
      Caption = 'TOTAL'
    end
    object lbl_harga: TLabel
      Left = 112
      Top = 112
      Width = 48
      Height = 13
      Caption = '00000000'
    end
    object edt_Dibayar: TEdit
      Left = 112
      Top = 160
      Width = 153
      Height = 21
      TabOrder = 0
      OnChange = edt_DibayarChange
    end
    object btn_Simpan: TButton
      Left = 24
      Top = 240
      Width = 75
      Height = 33
      Caption = 'OK'
      TabOrder = 1
      OnClick = btn_SimpanClick
    end
    object Button1: TButton
      Left = 144
      Top = 240
      Width = 73
      Height = 33
      Caption = 'KEMBALI'
      TabOrder = 2
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 24
      Top = 288
      Width = 73
      Height = 33
      Caption = 'SELESAI'
      TabOrder = 3
      OnClick = Button2Click
    end
  end
end
