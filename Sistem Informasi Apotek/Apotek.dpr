program Apotek;

uses
  Forms,
  uMain in 'uMain.pas' {form_main},
  uDM in 'uDM.pas' {DM: TDataModule},
  uLogin in 'uLogin.pas' {form_Login},
  uLogin2 in 'uLogin2.pas' {form_LoginAdmin},
  uMainAdmin in 'uMainAdmin.pas' {form_main_Admin},
  uLamanPertama in 'uLamanPertama.pas' {form_awal},
  uProsesTransaksi in 'uProsesTransaksi.pas' {Form_transaksi},
  rep in 'rep.pas' {Form1};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TDM, DM);
  Application.CreateForm(Tform_awal, form_awal);
  Application.CreateForm(Tform_Login, form_Login);
  Application.CreateForm(Tform_LoginAdmin, form_LoginAdmin);
  Application.CreateForm(Tform_main, form_main);
  Application.CreateForm(Tform_main_Admin, form_main_Admin);
  Application.CreateForm(TForm_transaksi, Form_transaksi);
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
