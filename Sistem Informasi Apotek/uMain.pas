unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ToolWin, ExtCtrls, Grids, DBGrids, ActnList,
  ImgList, sSkinManager, sButton, Buttons, sBitBtn, acPNG, Menus;

type
  Tform_main = class(TForm)
    page: TPageControl;
    page_master: TTabSheet;
    page_transaksi: TTabSheet;
    grubox_master_data_obat: TGroupBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    Button5: TButton;
    Button4: TButton;
    Button3: TButton;
    Label1: TLabel;
    sSkinManager1: TsSkinManager;
    Label2: TLabel;
    Image1: TImage;
    Label3: TLabel;
    Image2: TImage;
    edt_Stock: TEdit;
    edt_hargaObat: TEdit;
    edt_jenisObat: TEdit;
    edt_NamaObat: TEdit;
    edt_ID: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Panel4: TPanel;
    Image3: TImage;
    Label10: TLabel;
    Grubox_data_karyawan: TGroupBox;
    DBGrid2: TDBGrid;
    Panel5: TPanel;
    Image4: TImage;
    Panel6: TPanel;
    Panel7: TPanel;
    Label11: TLabel;
    Button1: TButton;
    Grubox_ubah_pass: TGroupBox;
    Panel8: TPanel;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    edt_username: TEdit;
    edt_Password: TEdit;
    edt_newPassword: TEdit;
    Button2: TButton;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Image9: TImage;
    Label15: TLabel;
    Edit1: TEdit;
    Button6: TButton;
    Label17: TLabel;
    Panel10: TPanel;
    grubox_transaksi_jual: TGroupBox;
    DBGrid3: TDBGrid;
    Label18: TLabel;
    Label20: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    TAMBAHKAN: TButton;
    PROSES: TButton;
    edt_Jenis: TEdit;
    edt_nmObat: TEdit;
    edt_idObat: TEdit;
    edt_Qty: TEdit;
    Harga: TLabel;
    Label16: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    lbl_karyawan: TLabel;
    Label30: TLabel;
    lbl_nama_Karyawan: TLabel;
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure Image2Click(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
    procedure Panel2Click(Sender: TObject);
    procedure E1Click(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Refresh;
    procedure RefreshKaryawan;
    procedure Button5Click(Sender: TObject);
    procedure Panel4Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Label10Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure Panel5Click(Sender: TObject);
    procedure Image4Click(Sender: TObject);
    procedure Label11Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Panel7Click(Sender: TObject);
    procedure Image9Click(Sender: TObject);
    procedure DBGrid3CellClick(Column: TColumn);
    procedure TAMBAHKANClick(Sender: TObject);
    procedure PROSESClick(Sender: TObject);
    procedure edt_QtyChange(Sender: TObject);
   
  

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  form_main: Tform_main;

implementation

uses uDM, Math, uLamanPertama, uProsesTransaksi, uLogin;

{$R *.dfm}


procedure Tform_main.Refresh;
begin
DM.DS.DataSet:=DM.ADO_Query;
DBGrid1.DataSource:=DM.DS;
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select * from tbl_Obat';
  Active:=True;
end;
end;
procedure Tform_main.RefreshKaryawan;
begin
DM.DS.DataSet:=DM.ADO_Query;
DBGrid2.DataSource:=DM.DS;
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select ID_Karyawan,Nama_Karyawan,JENIS_KELAMIN,TELP,ALAMAT from tbl_karyawan';
  Active:=True;
end;
end;
procedure Tform_main.Button6Click(Sender: TObject);
begin
Edit1.Text:='';

with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select * from tbl_Obat where nama like''%'+Edit1.Text+'%''';
  Active:=True;
end;
end;

procedure Tform_main.Button7Click(Sender: TObject);
begin
grubox_master_data_obat.Hide;
end;

procedure Tform_main.FormActivate(Sender: TObject);
begin
grubox_master_data_obat.Hide;
Grubox_data_karyawan.Hide;
Grubox_ubah_pass.Hide;
grubox_transaksi_jual.Hide;
Refresh;
Label19.Caption:=FormatDateTime('dd.mm.yyyy', now);
Label21.Caption:=FormatDateTime('dd.mm.yyyy', now);
 Left:=(Screen.Width-Width)  div 2;
 Top:=(Screen.Height-Height) div 2;

if (harga.Caption='00000000')then
begin
PROSES.Enabled:=false;
end;
end;



procedure Tform_main.Image1Click(Sender: TObject);
begin
Grubox_ubah_pass.Hide;
grubox_master_data_obat.Show;
Grubox_data_karyawan.Hide;
Refresh;
end;

procedure Tform_main.Image2Click(Sender: TObject);
begin
grubox_master_data_obat.Hide;
Grubox_ubah_pass.Show;
Grubox_data_karyawan.Hide;

end;

procedure Tform_main.Panel3Click(Sender: TObject);
begin
Grubox_ubah_pass.Hide;
grubox_master_data_obat.Show;
Grubox_data_karyawan.Hide;

Refresh
end;

procedure Tform_main.Panel2Click(Sender: TObject);
begin
grubox_master_data_obat.Hide;
Grubox_ubah_pass.Show;
Grubox_data_karyawan.Hide;

end;

procedure Tform_main.E1Click(Sender: TObject);
begin
form_main.Hide;
end;

procedure Tform_main.DBGrid1CellClick(Column: TColumn);
begin
edt_ID.Text:=DM.ADO_Query['ID_Obat'];
edt_NamaObat.Text:=DM.ADO_Query['Nama'];
edt_jenisObat.Text:=DM.ADO_Query['Jenis'];
edt_hargaObat.Text:=DM.ADO_Query['Harga'];
edt_Stock.Text:=DM.ADO_Query['Stock'];
edt_ID.Enabled:=False;
end;

procedure Tform_main.Button4Click(Sender: TObject);
begin
  If ((edt_ID.Text='') and (edt_NamaObat.Text='') and (edt_jenisObat.Text='') and (edt_hargaObat.Text='') and (edt_Stock.Text='')) then
  begin
  ShowMessage('Data Masih Kosong');
  end
  else
  try DM.ADO_Con.BeginTrans;
  with DM.ADO_Query do
  begin
  Active:=False;
  SQL.Clear;
  SQL.Text:='update tbl_obat set Nama='+QuotedStr(edt_NamaObat.Text)+
  ',Jenis='+QuotedStr(edt_jenisObat.Text)+
  ',Harga='+QuotedStr(edt_hargaObat.Text)+
  ',stock='+QuotedStr(edt_Stock.Text)+
  'where id_obat='+QuotedStr(edt_ID.Text);
  ExecSQL;
  end;
  DM.ADO_Con.CommitTrans;
  ShowMessage('Data Berhasil diubah');
  edt_ID.Text:='';
  edt_NamaObat.Text:='';
  edt_jenisObat.Text:='';
  edt_hargaObat.Text:='';
  edt_Stock.Text:='';
  Refresh;
  except
  ShowMessage('Data Gagal diubah');
  DM.ADO_Con.RollbackTrans;
  end;

end;
procedure Tform_main.Button3Click(Sender: TObject);
begin
If ((edt_ID.Text='') and (edt_NamaObat.Text='') and (edt_jenisObat.Text='') and (edt_hargaObat.Text='') and (edt_Stock.Text='')) then
  begin
  ShowMessage('Data Masih Kosong');
  end
else
  try DM.ADO_Con.BeginTrans;
  with DM.ADO_Query do
  begin
  Active:=False;
  SQL.Clear;
  SQL.Text:='insert into tbl_obat values('+QuotedStr(edt_ID.Text)+','+
  QuotedStr(edt_NamaObat.Text)+','+QuotedStr(edt_jenisObat.Text)+','+
  QuotedStr(edt_hargaObat.Text)+','+QuotedStr(edt_Stock.Text)+')';
  ExecSQL;
  end;
  DM.ADO_Con.CommitTrans;
  ShowMessage('Data Berhasil Disimpan');
  except
  ShowMessage('Data Gagal Disimpan');
  DM.ADO_Con.RollbackTrans;
  end;
  edt_ID.Text:='';
  edt_NamaObat.Text:='';
  edt_jenisObat.Text:='';
  edt_hargaObat.Text:='';
  edt_Stock.Text:='';
  Refresh;

end;

procedure Tform_main.Button5Click(Sender: TObject);
begin
If ((edt_ID.Text='') and (edt_NamaObat.Text='') and (edt_jenisObat.Text='') and (edt_hargaObat.Text='') and (edt_Stock.Text='')) then
  begin
  ShowMessage('Data Masih Kosong');
  end
else
  try DM.ADO_Con.BeginTrans;
  with DM.ADO_Query do
  begin
  Active:=False;
  SQL.Clear;
  SQL.Text:='delete from tbl_obat where id_obat='+QuotedStr(edt_ID.Text);
  ExecSQL;
  end;
  DM.ADO_Con.CommitTrans;
  ShowMessage('Data Berhasil Dihapus');
  except
  ShowMessage('Data Gagal Dihapus');
  DM.ADO_Con.RollbackTrans;
  end;
  edt_ID.Text:='';
  edt_NamaObat.Text:='';
  edt_jenisObat.Text:='';
  edt_hargaObat.Text:='';
  edt_Stock.Text:='';
  Refresh;
end;

procedure Tform_main.Panel4Click(Sender: TObject);
begin
grubox_master_data_obat.Hide;
Grubox_data_karyawan.Show;
Grubox_ubah_pass.Hide;

RefreshKaryawan;
end;

procedure Tform_main.Label2Click(Sender: TObject);
begin
Grubox_ubah_pass.Hide;
grubox_master_data_obat.Show;
Grubox_data_karyawan.Hide;
end;

procedure Tform_main.Label10Click(Sender: TObject);
begin
Grubox_ubah_pass.Hide;
grubox_master_data_obat.Hide;
Grubox_data_karyawan.Show;
RefreshKaryawan;
end;

procedure Tform_main.Image3Click(Sender: TObject);
begin
Grubox_ubah_pass.Hide;
grubox_master_data_obat.Hide;
Grubox_data_karyawan.Show;
RefreshKaryawan;
end;

procedure Tform_main.Label3Click(Sender: TObject);
begin
grubox_master_data_obat.Hide;
Grubox_ubah_pass.Show;
Grubox_data_karyawan.Hide;

end;

procedure Tform_main.Panel5Click(Sender: TObject);
begin
form_awal.Show;
form_main.Hide;
end;

procedure Tform_main.Image4Click(Sender: TObject);
begin
form_awal.Show;
form_main.Hide;
end;

procedure Tform_main.Label11Click(Sender: TObject);
begin
form_awal.Show;
form_main.Hide;
end;

procedure Tform_main.Button1Click(Sender: TObject);
begin
edt_ID.Text:='';
edt_NamaObat.Text:='';
edt_jenisObat.Text:='';
edt_hargaObat.Text:='';
edt_Stock.Text:='';
edt_ID.Enabled:=True;
end;

procedure Tform_main.Button2Click(Sender: TObject);
begin
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select*from tbl_karyawan';
  Active:=True;
  if (edt_username.Text=DM.ADO_Query['username'] )and (edt_Password.Text=DM.ADO_Query['password'] ) then
    begin
      SQL.Text:='update tbl_karyawan set Password='+QuotedStr(edt_newPassword.Text)+
      'where username='+QuotedStr(edt_username.Text);
      ShowMessage('Ubah Pass Sukses');
      edt_username.Text:='';
      edt_Password.Text:='';
      edt_newPassword.Text:='';
    end
  else
    begin
      ShowMessage('Gagal Ubah Pass');
      ShowMessage('Ubah Pass Sukses');
      edt_username.Text:='';
      edt_Password.Text:='';
      edt_newPassword.Text:='';
    end;
  ExecSQL;


end;
end;



procedure Tform_main.Panel7Click(Sender: TObject);
begin
grubox_transaksi_jual.Show;
DM.DS.DataSet:=DM.ADO_Query;
DBGrid3.DataSource:=DM.DS;
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select * from tbl_Obat';
  Active:=True;
end;

with DM.ADO_Query1 do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select Nama_Karyawan from tbl_Karyawan where username like''%'+form_login.Username.Text+'%''';
  Active:=True;
end;
lbl_nama_Karyawan.Caption:=(DM.ADO_Query1['Nama_Karyawan'])

end;

procedure Tform_main.Image9Click(Sender: TObject);
begin
grubox_transaksi_jual.Show;
DM.DS.DataSet:=DM.ADO_Query;
DBGrid3.DataSource:=DM.DS;
DM.ADO_Query.Connection:=DM.ADO_Con;
with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select * from tbl_Obat';
  Active:=True;
end;
with DM.ADO_Query1 do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='select Nama_Karyawan from tbl_Karyawan where username like''%'+form_login.Username.Text+'%''';
  Active:=True;
end;
lbl_nama_Karyawan.Caption:=(DM.ADO_Query1['Nama_Karyawan'])
end;

procedure Tform_main.DBGrid3CellClick(Column: TColumn);
begin
edt_idObat.Text:=DM.ADO_Query['ID_Obat'];
edt_nmObat.Text:=DM.ADO_Query['Nama'];
edt_Jenis.Text:=DM.ADO_Query['Jenis'];
Harga.Caption:=DM.ADO_Query['Harga'];

edt_idObat.Enabled:=False;
edt_Jenis.Enabled:=False;
edt_nmObat.Enabled:=False;
end;

procedure Tform_main.TAMBAHKANClick(Sender: TObject);
begin
If ((edt_idObat.Text='') and (edt_Qty.Text='')) then
  begin
  ShowMessage('Data Masih Kosong');

  end
else if ((edt_Qty.Text='')) then
  begin
  ShowMessage('Isi data QTY');
  end
else
try DM.ADO_Con.BeginTrans;
with DM.ADO_Query do
begin
  Active:=False;
  Close;
  SQL.Clear;
  SQL.Text:='insert into Transaksi_Jual (Id_Obat, nama_Obat, Jenis, Tanggal, QTY, Satuan, Total_Harga, Nama_Karyawan ) values('+QuotedStr(edt_idObat.Text)+','+
  QuotedStr(edt_nmObat.Text)+','+
  QuotedStr(edt_Jenis.Text)+','+
  QuotedStr(Label21.Caption)+','+
  QuotedStr(edt_Qty.Text)+','+
  QuotedStr(Harga.Caption)+','+
  QuotedStr(Label16.Caption)+','+
  QuotedStr(lbl_nama_Karyawan.Caption)+')';
  ExecSQL;
end;
DM.ADO_Con.CommitTrans;
  ShowMessage('Data Berhasil Ditambahkan');
  edt_idObat.Text:='';
  edt_nmObat.Text:='';
  edt_Jenis.Text:='';
  //Harga.Caption:='000000';
  edt_Qty.Text:='0';
  //label16.Caption:='000000';
  except
  ShowMessage('Data Gagal Ditambahkan');
  DM.ADO_Con.RollbackTrans;
  edt_idObat.Text:='';
  edt_nmObat.Text:='';
  edt_Jenis.Text:='';
  //Harga.Caption:='000000';
  edt_Qty.Text:='0';
  //label16.Caption:='000000';
  end;


Refresh;
end;

procedure Tform_main.PROSESClick(Sender: TObject);
begin
form_transaksi.Show;
end;

procedure Tform_main.edt_QtyChange(Sender: TObject);
var a,b,c:integer;
begin
PROSES.Enabled:=True;
a:=StrToInt(Harga.Caption);
b:=StrToInt(edt_Qty.Text);
c:=a*b;
Label16.Caption:=IntToStr(c);
end;

end.
